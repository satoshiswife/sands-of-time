// dependencies
const path = require("path");
const http = require("http");
const express = require("express");
const socketIO = require("socket.io");
const Twit = require("twit");
const novelcovid = require("novelcovid");

let totaldeaths;

novelcovid.yesterday.countries({ country: "usa" }).then((value) => {
  ytotaldeaths = value.todayDeaths;
});

novelcovid.countries({ country: "usa" }).then((value) => {
  totaldeaths = value.deaths;
});

novelcovid.countries({ country: "usa" }).then((value) => {
  usa = value;
});

var T = new Twit({
  consumer_key: "HwtwMUM1IRFObUZET85wnk7Nz",
  consumer_secret: "dmEvHZS5FOT9mPTmjAciKkACMA4IerbUVKgjwES6NzrgHglycv",
  access_token: "996403556881072128-hJHbK5KsVYquo85IkAPpzcdQrrg4G39",
  access_token_secret: "xgJM1BiceSO3Ueip5jDPWwiTJrnihkE0XCE4LzB8kcoJd",
  timeout_ms: 5000,
  strictSSL: false,
});

// lat/log box around the USA
let usa = ["-126.69,24.08,-66.35,49.47"];

/*
//A tweet stream of everything related to  @realDonaldTrump 
// including:
// Tweets created by the user.
// Tweets which are retweeted by the user.
// Replies to any Tweet created by the user.
// Retweets of any Tweet created by the user.
// Manual replies, created without pressing a reply button (e.g. “@twitterapi I agree”).
*/
let stream = T.stream("statuses/filter", {
  // track: ["#MAGA", "#TRUMP2020", "@realDonaldTrump"],
   follow: [25073877],
   location: usa,
});

// set path to client side files
const publicPath = path.join(__dirname, "/../public");

// lets use espress to make things easier
let app = express();

// serve files from pubic dir set above
app.use(express.static(publicPath));

// start server
let httpServer = http.createServer(app);

// allow our server to access the socket.io lib
let io = socketIO(httpServer);

// listen on https/http port
httpServer.listen(process.env.PORT || 3000, function () {
  console.log("SERVER STARTED PORT: 3000");
});

// when someone connects
io.on("connection", (socket) => {
  console.log("socket connected!");
  socket.emit("sendDeaths", totaldeaths);

  // start twitter stream
  stream.start();
  socket.on("start stream", () => {
    console.log("start stream");

    // broadcast yesterdays total deaths
    socket.emit("sendYesterDeaths", ytotaldeaths);

    // when a tweet is received, push it to connect clients
    stream.on("tweet", function (tweet) {
      // send the stream data/tweets
      socket.emit("sendTweets", tweet);
    });
  });

  // when someone disconnects
  socket.on("disconnect", () => {
    console.log("socket disconnected!");
  });
});
