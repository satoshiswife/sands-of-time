<h1>The MAGA Times</h1> 

Social media has created a landscape where anyone can be a journalist. With services like Twitter and 
Facebook, the individual can easily spread information to many. When this individual holds a position of 
power, they have the ability to spread unchecked and false information to millions of people. The twitter 
account of Donald Trump (@realDonaldTrump) has over 88 million followers. With retweets and sharing, 
every tweet he sends out is seen by hundreds of millions of people. 

The MAGA Times is a data visualization of #MAGA and Covid-19 within the USA. Using the Twitter API, 
any activity happening on @realDonaldTrump is streamed in real time. Displayed as newsprint styled 
columns. On top of this is an illustration of Donald Trump. Every 10 to 30 seconds, the current tweet being streamed is read aloud with text to speech synthesis using the P5.speech library.

By pulling in COVID-19 related data from Open Disease Data we can calculate how many deaths happened 
per minute in the United States. Using this value as a timer, a drop of blood appears and flows down the 
screen everytime someone dies of the novel coronavirus and COVID-19 related symptoms.

The result is a strong visualization of how often people are dying from COVID-19 juxtaposed by a 
never-ending stream of tweets related to or sent by Donald Trump. Where the people continously spreading #MAGA messaging are seemingly oblivious to the reality around them.

Data Sources: Open Disease Data (https://disease.sh) & Twitter (https://developer.twitter.com)

Image Source: "Under Control" by Brian Stauffer (https://www.newyorker.com/magazine/2020/03/09)